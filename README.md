## Config

Node version: 20.14.0
Yarn
ExpresssJs
Typescript
Mysql

## Env config

cp .env.development .env
_Change the mysql configuration to match your local configuration_

## Script to install and run

yarn install
yarn dev

## Describe

This is a project with simple functions such as registering, adding, editing, and deleting users. Authenticate using JWT. Users can perform "guess the number" game information query operations. Users can perform "guess the number" game information query operations. such as adding, editing, deleting, getting information about one or more games, playing games by guessing numbers.
