import morgan from 'morgan';

import loggerFactory from '../lib/logger-factory';

const logger = loggerFactory.getLogger(__filename);

// Define a stream to redirect Morgan logs to winston
const morganStream = {
  write: (message: string) => {
    logger.info(message.trim());
  }
};

// Create a Morgan middleware instance
const morganMiddleware = morgan('dev', { stream: morganStream });

export default morganMiddleware;
