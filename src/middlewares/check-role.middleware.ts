import { UnauthorizedError } from 'routing-controllers';

export const checkRole = (allowedRoles: string) => {
  return (req, res, next) => {
    const role: string = req.user.role;

    if (allowedRoles.includes(role)) {
      return next();
    } else {
      throw new UnauthorizedError('Unauthorized');
    }
  };
};
