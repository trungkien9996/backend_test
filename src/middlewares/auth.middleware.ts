import 'reflect-metadata';

import { verify } from 'jsonwebtoken';
import * as _ from 'lodash';
import { Action, ForbiddenError, UnauthorizedError } from 'routing-controllers';

import { UserRole } from '../common/enum';
import { SECRET_KEY } from '../config';
import { DataStoredInToken } from '../interfaces/auth.interface';
import { AuthService } from '../services/auth.service';

const publicApiList = [];

export function authorizationChecker(): (
  action: Action,
  roles: number[]
) => Promise<boolean> | boolean {
  return async function innerAuthorizationChecker(
    action: Action,
    roles: number[]
  ): Promise<boolean> {
    try {
      const url = action.request.route.path;
      const method = action.request.method;

      const api = _.find(publicApiList, { url: url, method: method });

      const token = action.request.headers['authorization']
        ? action.request.headers['authorization'].split('Bearer ')[1]
        : null;

      if (api && !token) {
        const isPublic = Reflect.getMetadata('publicApi', api.prototype, api.key);

        if (isPublic) return true;
      }
      if (!token) throw new UnauthorizedError('Authentication token missing');

      const payload: DataStoredInToken = verify(token, SECRET_KEY) as DataStoredInToken;

      if (!payload) throw new ForbiddenError('Token invalid');
      const userId = payload?.id;
      const role = payload.role;
      let user;

      const authService = new AuthService();
      if (role === UserRole.Admin) {
        user = await authService.getAdminById(userId);
      } else {
        user = await authService.getUserByToken(userId);
      }

      if (!user) return false;
      action.request.user = user;
      if (!roles.length) return true;
      if (roles.indexOf(user.role) === -1) throw new ForbiddenError('Forbidden denied');
      return true;
    } catch (err) {
      tokenErrorResponse(err);
    }
  };
}

function tokenErrorResponse(err: any) {
  if (err?.name === 'TokenExpiredError') {
    throw new UnauthorizedError('Token expired');
  }
  if (err?.name === 'JsonWebTokenError') {
    throw new ForbiddenError('Token malformed');
  }
  if (err?.name === 'ForbiddenError') {
    throw new ForbiddenError(err?.message);
  }
  throw new UnauthorizedError('Authentication token missing');
}
