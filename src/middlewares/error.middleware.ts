import { ExpressErrorMiddlewareInterface, Middleware } from 'routing-controllers';

import { HttpException } from '../exceptions/HttpException';

@Middleware({ type: 'after' })
export class HttpErrorHandler implements ExpressErrorMiddlewareInterface {
  error(error: any, request: any, response: any, next: (err: any) => any) {
    if (error instanceof HttpException) {
      response.status(error.status).json({
        message: error.message,
        errorStatus: error.status
      });
    } else if (error.errors && Array.isArray(error.errors) && error?.httpCode === 400) {
      response.status(error.httpCode).json({
        errorStatus: error.httpCode,
        errors: error.errors.map((e) => Object.values(e.constraints)[0])
      });
    } else {
      response.status(error?.httpCode || 500).json({
        errorStatus: error?.httpCode || 500,
        errors: error
      });
    }
    next(error);
  }
}
