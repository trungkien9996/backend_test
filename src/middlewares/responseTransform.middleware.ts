import { Action, Interceptor, InterceptorInterface } from 'routing-controllers';
import { Model } from 'sequelize';

@Interceptor()
export class ResponseInterceptor implements InterceptorInterface {
  intercept(action: Action, content: any) {
    try {
      if (content instanceof Model) {
        return {
          data: content.get(),
          statusCode: action.response.statusCode
        };
      } else if (Array.isArray(content) && content[0] === Model) {
        return {
          data: content.map((e: Model) => e.get()),
          statusCode: action.response.statusCode
        };
      } else if (content[`count`] && content[`rows`]) {
        return {
          count: content[`count`],
          data: content[`rows`],
          statusCode: action.response.statusCode
        };
      }
      return {
        data: content,
        statusCode: action.response.statusCode
      };
    } catch {
      return {
        data: content,
        statusCode: action.response.statusCode
      };
    }
  }
}
