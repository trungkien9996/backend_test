import 'reflect-metadata';

import { validationMetadatasToSchemas } from 'class-validator-jsonschema';
import express, { Application } from 'express';
import mysql from 'mysql2/promise';
import { createExpressServer, getMetadataArgsStorage } from 'routing-controllers';
import { routingControllersToSpec } from 'routing-controllers-openapi';
import * as swaggerUi from 'swagger-ui-express';

import { API_PORT, DB_HOST, DB_NAME, DB_PASSWORD, DB_PORT, DB_USERNAME } from './config';
import { AuthController } from './controllers/auth.controller';
import { GamesController } from './controllers/game.controller';
import { UsersController } from './controllers/users.controller';
import { AppDataSource } from './database/data-source';
import loggerFactory from './lib/logger-factory';
import { authorizationChecker } from './middlewares/auth.middleware';
import { HttpErrorHandler } from './middlewares/error.middleware';
import morganMiddleware from './middlewares/morgan.middleware';
import { ResponseInterceptor } from './middlewares/responseTransform.middleware';
import { runSeed } from './model/seeds/seed';

const logger = loggerFactory.getLogger(__filename);

const controllers = [AuthController, UsersController, GamesController];

AppDataSource.initialize()
  .then(async () => {
    logger.info('Data Source has been initialized!');
  })
  .catch(async (err) => {
    if (err.toString().includes(`Error: Unknown database '${DB_NAME}'`)) {
      const connection = await mysql.createConnection({
        host: DB_HOST,
        port: DB_PORT,
        user: DB_USERNAME,
        password: DB_PASSWORD
      });

      await connection.query(`CREATE DATABASE IF NOT EXISTS ${DB_NAME}`);
      await connection.end();

      await AppDataSource.initialize();
    }
  })
  .then(async () => {
    await runSeed();
  });

const expressApp: Application = createExpressServer({
  routePrefix: '/api/',
  controllers: controllers,
  defaultErrorHandler: false,
  classTransformer: true,
  interceptors: [ResponseInterceptor],
  cors: {
    origin: '*',
    credentials: true
  },
  middlewares: [
    express.json(),
    express.urlencoded({ extended: true }),
    // cookieParser(),
    HttpErrorHandler
  ],
  authorizationChecker: authorizationChecker()
});

expressApp.use(morganMiddleware);

const schemas = validationMetadatasToSchemas({
  refPointerPrefix: '#/components/schemas/'
});

const storage = getMetadataArgsStorage();

const swaggerSpec = routingControllersToSpec(
  storage,
  {
    routePrefix: '/api/'
  },
  {
    info: {
      title: 'backend_test API',
      version: '1.0',
      description: 'backend_test API'
    },
    components: {
      schemas: schemas as any,
      securitySchemes: {
        bearerAuth: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT'
        }
      }
    },
    security: [{ bearerAuth: [] }]
  }
);

expressApp.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

expressApp.listen(API_PORT, () => {
  logger.info(`=================================`);
  logger.info(`🚀 App listening on the port ${API_PORT}`);
  logger.info(`=================================`);
});
