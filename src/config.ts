import * as dotenv from 'dotenv';
dotenv.config();

// DB configure

export const { API_PORT, DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME } = process.env;

export const DB_PORT = Number(process.env.DB_PORT);

export const DB_TIME_FORMAT = 'YYYY-MM-DD HH:mm:ss';

// security

export const { SECRET_KEY, REFRESH_SECRET_KEY } = process.env;

// admin user

export const { ADMIN_EMAIL, ADMIN_PASSWORD } = process.env;

export const DEFAULT_DATE_FORMAT = 'YYYY-MM-DD';
