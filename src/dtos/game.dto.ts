import { Type } from 'class-transformer';
import { IsBoolean, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { JSONSchema } from 'class-validator-jsonschema';

import { Pagination } from '../common/dtos';

export class UpdateGameDto {
  @IsOptional()
  @IsBoolean()
  @JSONSchema({ type: 'boolean', example: false })
  success?: boolean;

  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  @JSONSchema({ type: 'number', example: 1 })
  attempts?: number;
}

export class CreateGameDto extends UpdateGameDto {
  @IsNotEmpty()
  @JSONSchema({ type: 'string', example: 'game1' })
  name: string;

  @IsNotEmpty()
  @Type(() => Number)
  @IsNumber()
  @JSONSchema({ type: 'number', example: 10 })
  targetNumber: number;
}

export class FindMany extends Pagination {
  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  @JSONSchema({ type: 'number' })
  attempts?: number;

  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  @JSONSchema({ type: 'number' })
  targetNumber?: number;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @JSONSchema({ type: 'string' })
  name?: string;

  @IsOptional()
  @IsBoolean()
  @JSONSchema({ type: 'boolean' })
  success?: boolean;
}

export class PlayGame {
  @IsNotEmpty()
  @Type(() => Number)
  @IsNumber()
  @JSONSchema({ type: 'number', example: 10 })
  guess: number;
}
