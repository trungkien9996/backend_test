import { Type } from 'class-transformer';
import {
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
  MinLength
} from 'class-validator';
import { JSONSchema } from 'class-validator-jsonschema';

import { EGender, UserRole } from '../common/enum';

export class UpdateUserDto {
  @IsNotEmpty()
  @IsString()
  @MinLength(2)
  @MaxLength(100)
  @JSONSchema({ type: 'string', example: 'sample' })
  fullname: string;

  @IsNotEmpty()
  @IsEmail()
  @JSONSchema({ type: 'string', example: 'sample@gmail.com' })
  email: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(8)
  @MaxLength(100)
  @JSONSchema({ type: 'string', example: '12345678' })
  password: string;

  @IsOptional()
  @IsString()
  @JSONSchema({ type: 'string', nullable: true, example: '+121452342' })
  phone_number?: string;

  @IsOptional()
  @IsEnum(EGender)
  @JSONSchema({ type: 'string', example: EGender.Male })
  sex?: EGender;

  @IsNotEmpty()
  @IsEnum(UserRole)
  @Type(() => Number)
  @JSONSchema({ type: 'number', example: UserRole.Admin })
  role: UserRole;

  @IsOptional()
  @IsString()
  @JSONSchema({
    type: 'string',
    example:
      'https://blog.openreplay.com/images/how-to-perform-jwt-authentication-with-react/images/hero.png'
  })
  image?: string;
}

export class CreateUserDto extends UpdateUserDto {
  @IsNotEmpty()
  @IsString()
  @MinLength(2)
  @MaxLength(100)
  @JSONSchema({ type: 'string', example: 'sample' })
  username: string;
}

export class VerifyAccountDeletionDto {
  @IsString()
  @JSONSchema({ type: 'string', example: 'superadmin' })
  password: string;
}
