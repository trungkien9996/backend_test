import { IsEmail, IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';
import { JSONSchema } from 'class-validator-jsonschema';

export class AuthDto {
  @IsNotEmpty()
  @IsEmail()
  @JSONSchema({ type: 'string', example: 'superadmin@gmail.com' })
  public email: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(8)
  @MaxLength(100)
  @JSONSchema({ type: 'string', example: 'superadmin' })
  public password: string;
}

export class VerifyEmailDto {
  @IsNotEmpty()
  @IsEmail()
  @JSONSchema({ type: 'string', example: 'sample@gmail.com' })
  public email: string;
}

export class ForgotUsernameDto {
  @IsNotEmpty()
  @IsString()
  @MinLength(2)
  @MaxLength(100)
  @JSONSchema({ type: 'string', example: 'sample' })
  fullname: string;

  @IsNotEmpty()
  @IsEmail()
  @JSONSchema({ type: 'string', example: 'sample@gmail.com' })
  email: string;
}

export class ForgotPasswordDto {
  @IsNotEmpty()
  @IsString()
  @MinLength(2)
  @MaxLength(100)
  @JSONSchema({ type: 'string', example: 'sample' })
  fullname: string;

  @IsNotEmpty()
  @IsEmail()
  @JSONSchema({ type: 'string', example: 'sample@gmail.com' })
  email: string;
}
