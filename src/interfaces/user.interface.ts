import { EGender, UserRole } from '../common/enum';
import { BaseAttributes } from '../common/interface';
import { GameAttribute } from './game.interface';

export interface UserAttribute extends BaseAttributes {
  fullname: string;
  password: string;
  email: string;
  username: string;
  phone_number?: string;
  sex?: EGender;
  image?: string;
  role: UserRole;
  last_access_at: Date;
  games?: GameAttribute[];
}
