import { BaseAttributes } from '../common/interface';
import { UserAttribute } from './user.interface';

export interface GameAttribute extends BaseAttributes {
  user: UserAttribute;
  name: string;
  targetNumber: number;
  attempts: number;
  success: boolean;
}
