export interface DataStoredInToken {
  id: number;
  role: number;
}

export interface TokenData {
  token: string;
  expiresIn: number;
}
