import {
  Authorized,
  Body,
  Delete,
  Get,
  JsonController,
  Param,
  Patch,
  Post,
  QueryParams,
  Req
} from 'routing-controllers';

import { CreateGameDto, FindMany, PlayGame, UpdateGameDto } from '../dtos/game.dto';
import { GameService } from '../services/game.service';

@JsonController('games')
@Authorized()
export class GamesController {
  private gameService = new GameService();
  @Post()
  public async create(@Body() createUserDto: CreateGameDto, @Req() req: any) {
    return this.gameService.createGame(createUserDto, req.user);
  }

  @Post('/makeGuess/:id')
  public async playGame(@Param('id') id: number, @Body() data: PlayGame) {
    return this.gameService.makeGuess(id, data.guess);
  }

  @Get('/:id')
  async findOneById(@Param('id') id: number, @Req() req: any) {
    return this.gameService.findGameById(id, req.user.id);
  }

  @Get('')
  async findMany(@QueryParams() filter: FindMany) {
    return this.gameService.findManyByCondition(filter);
  }

  @Patch('/:id')
  async updateGameById(
    @Param('id') id: number,
    @Body() updateUserDto: UpdateGameDto,
    @Req() req: any
  ) {
    return this.gameService.updateOneGame(updateUserDto, id, req.user.id);
  }

  @Delete('/:id')
  async deleteById(@Param('id') id: number, @Req() req: any) {
    return this.gameService.deleteGame(id, req.user.id);
  }
}
