import 'reflect-metadata';

import bodyParser from 'body-parser';
import { Body, JsonController, Post, UseBefore } from 'routing-controllers';

import { AuthDto } from '../dtos/auth.dto';
import { CreateUserDto } from '../dtos/user.dto';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';

@JsonController('auth')
@UseBefore(bodyParser.urlencoded({ extended: false }))
export class AuthController {
  private readonly authService = new AuthService();
  private readonly userService = new UserService();

  @Post('/login')
  public async login(@Body() authDto: AuthDto) {
    return await this.authService.loginByPass(authDto);
  }

  @Post('/register')
  public async register(@Body() data: CreateUserDto) {
    return this.userService.createUser(data);
  }
}
