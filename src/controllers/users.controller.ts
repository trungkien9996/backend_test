import 'reflect-metadata';

import { Authorized, Body, Get, JsonController, Patch, Post, Req } from 'routing-controllers';

import { CreateUserDto, UpdateUserDto, VerifyAccountDeletionDto } from '../dtos/user.dto';
import { UserService } from '../services/user.service';

@JsonController('users')
@Authorized()
export class UsersController {
  private userService = new UserService();
  @Post()
  async create(@Body() createUserDto: CreateUserDto, @Req() req: any) {
    return this.userService.createUser(createUserDto, req.user.id);
  }

  @Get('')
  async getProfile(@Req() req: any) {
    return this.userService.getProfile(req.user.id);
  }

  @Patch('')
  async updateProfile(@Body() updateUserDto: UpdateUserDto, @Req() req: any) {
    return this.userService.updateUser(updateUserDto, req.user.id);
  }

  @Post('/delete/verify-password')
  public async verifyPassword(@Body() data: VerifyAccountDeletionDto, @Req() req: any) {
    return this.userService.deleteUser(data.password, req.user.id);
  }
}
