import { createHash } from 'node:crypto';

import { sign } from 'jsonwebtoken';

import { REFRESH_SECRET_KEY, SECRET_KEY } from '../config';
import { AppDataSource } from '../database/data-source';
import { AuthDto } from '../dtos/auth.dto';
import { HttpException } from '../exceptions/HttpException';
import { DataStoredInToken } from '../interfaces/auth.interface';
import { UserAttribute } from '../interfaces/user.interface';
import loggerFactory from '../lib/logger-factory';
import { User } from '../model/user.entity';

export class AuthService {
  logger = loggerFactory.getLogger(AuthService.name);

  userRepository = AppDataSource.getRepository(User);

  public async loginByPass(data: AuthDto) {
    const user = await this.userRepository.findOne({
      where: {
        email: data.email
      }
    });

    if (!user) {
      throw new HttpException(404, 'User does not exist');
    }

    if (user.password !== createHash('sha256').update(data.password).digest('hex')) {
      throw new HttpException(403, 'Password does not correct');
    }

    await this.userRepository.update({ last_access_at: new Date() }, { email: data.email });

    const [authToken, rfToken] = await Promise.all([
      this.createToken(user),
      this.createRefreshToken(user)
    ]);

    const result = {
      user: user,
      authToken: authToken,
      rfToken: rfToken
    };

    return result;
  }

  private async createToken(user: any) {
    const dataStoredInToken: DataStoredInToken = {
      id: user.id,
      role: user.role
    };
    const secretKey: string = SECRET_KEY;
    //Token expire 1 hour
    const expiresIn: number = 60 * 60;
    const authToken = sign(dataStoredInToken, secretKey, { expiresIn });
    return authToken;
  }

  private async createRefreshToken(user: any) {
    const dataStoredInToken: DataStoredInToken = {
      id: user.id,
      role: user.role
    };
    const secretKey: string = REFRESH_SECRET_KEY;
    //Token expire 1 year
    const expiresIn: number = 60 * 60 * 24 * 365;
    const refreshToken = sign(dataStoredInToken, secretKey, { expiresIn });
    return refreshToken;
  }

  public async getAdminById(id: number): Promise<UserAttribute> {
    const admin = await this.userRepository.findOneBy({
      id: id
      // role: Number(UserRole.Admin)
    });
    return admin;
  }

  public async getUserByToken(id: number): Promise<UserAttribute> {
    const admin = await this.userRepository.findOneBy({
      id: id
      // role: UserRole.Member
    });
    return admin;
  }
}
