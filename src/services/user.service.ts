import 'reflect-metadata';

import { createHash } from 'node:crypto';

import { AppDataSource } from '../database/data-source';
import { CreateUserDto, UpdateUserDto } from '../dtos/user.dto';
import { HttpException } from '../exceptions/HttpException';
import { UserAttribute } from '../interfaces/user.interface';
import loggerFactory from '../lib/logger-factory';
import { User } from '../model/user.entity';

export class UserService {
  logger = loggerFactory.getLogger(UserService.name);

  userRepository = AppDataSource.getRepository(User);

  async createUser(data: CreateUserDto, userId?: number) {
    await this.checkDuplicateInfo(data);

    const passwordHash = this.hashPassword(data.password);

    const queryRunner = AppDataSource.createQueryRunner();

    await queryRunner.startTransaction();

    let savedUser: UserAttribute;
    try {
      const newUser = await this.userRepository.create({
        ...data,
        password: passwordHash,
        createdBy: userId | 1,
        updatedBy: userId | 1
      });

      savedUser = await this.userRepository.save(newUser);
      await queryRunner.commitTransaction();
    } catch (error) {
      await queryRunner.rollbackTransaction();
      this.logger.error(error);
    } finally {
      await queryRunner.release();
    }
    return savedUser;
  }

  async getProfile(userId: number) {
    const user = await this.userRepository.findOne({ where: { id: userId } });
    return user;
  }

  async checkNullTable() {
    return await this.userRepository.findOne({});
  }

  async updateUser(data: UpdateUserDto, userId: number) {
    const user = await this.userRepository.findOne({ where: { id: userId } });
    if (!user) throw new HttpException(400, 'User does not exist');

    if (data.email !== user.email) {
      await this.checkDuplicateEmail(data.email);
    }

    const updateOptions = {
      ...data
    };

    if (data.password) {
      const passwordHash = this.hashPassword(data.password);

      updateOptions.password = passwordHash;
    }

    const queryRunner = AppDataSource.createQueryRunner();

    await queryRunner.startTransaction();
    let savedUser: UserAttribute;
    try {
      Object.assign(user, updateOptions);
      savedUser = await this.userRepository.save(user);

      await queryRunner.commitTransaction();
    } catch (error) {
      await queryRunner.rollbackTransaction();
      this.logger.error(error);
    } finally {
      await queryRunner.release();
    }
    return savedUser;
  }

  async deleteUser(password: string, userId: number) {
    const user = await this.userRepository.findOne({
      where: {
        id: userId
      }
    });

    if (!user) {
      throw new HttpException(404, 'User does not exist');
    }

    if (this.hashPassword(password) !== user.password) {
      throw new HttpException(400, 'Wrong password');
    }

    await this.userRepository.remove(user);

    return;
  }

  private async checkDuplicateEmail(email: string) {
    const user = await this.userRepository.findOne({ where: { email: email } });
    if (user) throw new HttpException(400, 'Email already exist');
  }

  private async checkDuplicateInfo(data: Pick<CreateUserDto, 'username' | 'email'>) {
    if (data.email) {
      const user = await this.userRepository.findOne({ where: { email: data.email } });
      if (user) throw new HttpException(400, 'Email already exist');
    }
    if (data.username) {
      const user = await this.userRepository.findOne({ where: { username: data.username } });
      if (user) throw new HttpException(400, 'Username already exist');
    }
  }
  private hashPassword(password: string) {
    const newPass = createHash('sha256').update(password).digest('hex');
    return newPass;
  }
}
