import { FindOptionsWhere } from 'typeorm';

import { AppDataSource } from '../database/data-source';
import { CreateGameDto, FindMany, UpdateGameDto } from '../dtos/game.dto';
import { HttpException } from '../exceptions/HttpException';
import { GameAttribute } from '../interfaces/game.interface';
import { UserAttribute } from '../interfaces/user.interface';
import loggerFactory from '../lib/logger-factory';
import { Game } from '../model/game.entity';

export class GameService {
  logger = loggerFactory.getLogger(GameService.name);
  gameRepository = AppDataSource.getRepository(Game);

  async createGame(data: CreateGameDto, user: UserAttribute): Promise<GameAttribute> {
    const queryRunner = AppDataSource.createQueryRunner();

    await queryRunner.startTransaction();

    let savedGame: GameAttribute;
    try {
      const newUser = await this.gameRepository.create({
        ...data,
        user: user,
        createdBy: user.id,
        updatedBy: user.id
      });

      savedGame = await this.gameRepository.save(newUser);
      await queryRunner.commitTransaction();
    } catch (error) {
      await queryRunner.rollbackTransaction();
      this.logger.error(error);
    } finally {
      await queryRunner.release();
    }
    return savedGame;
  }

  async findGameById(id: number, userId: number): Promise<GameAttribute> {
    const game = await this.gameRepository.findOneBy({ id: id, createdBy: userId });

    if (!game) {
      throw new HttpException(404, 'Game does not exist');
    }

    return game;
  }

  async findManyByCondition(searchGameDTO: FindMany): Promise<any> {
    const whereCondition: FindOptionsWhere<Game> = {};

    if (searchGameDTO.attempts) whereCondition.attempts = searchGameDTO.attempts;

    if (searchGameDTO.success) whereCondition.success = searchGameDTO.success;

    return await this.gameRepository.find({
      where: whereCondition,
      take: searchGameDTO.limit,
      skip: (searchGameDTO.page - 1) * searchGameDTO.limit
    });
  }

  async updateOneGame(data: UpdateGameDto, gameId: number, userId: number) {
    const game = await this.gameRepository.findOne({ where: { id: gameId, createdBy: userId } });
    if (!game) throw new HttpException(400, 'Game does not exist');

    const updateOptions = {
      ...data
    };

    const queryRunner = AppDataSource.createQueryRunner();

    await queryRunner.startTransaction();

    let savedGame: GameAttribute;

    try {
      Object.assign(game, updateOptions);
      savedGame = await this.gameRepository.save(game);

      await queryRunner.commitTransaction();
    } catch (error) {
      await queryRunner.rollbackTransaction();
      this.logger.error(error);
    } finally {
      await queryRunner.release();
    }
    return savedGame;
  }

  async deleteGame(gameId: number, userId: number) {
    const game = await this.gameRepository.findOne({
      where: {
        id: gameId,
        createdBy: userId
      }
    });

    if (!game) {
      throw new HttpException(400, 'User does not exist');
    }

    await this.gameRepository.remove(game);

    return;
  }

  async makeGuess(gameId: number, guess: number): Promise<string> {
    const game = await this.gameRepository.findOne({ where: { id: gameId }, relations: ['user'] });

    if (!game) {
      throw new Error('Game not found');
    }

    game.attempts += 1;

    if (guess === game.targetNumber) {
      game.success = true;
      await this.gameRepository.save(game);
      return 'Congratulations! You guessed the correct number.';
    } else if (guess > game.targetNumber) {
      await this.gameRepository.save(game);
      return 'Your guess is too high.';
    } else {
      await this.gameRepository.save(game);
      return 'Your guess is too low.';
    }
  }
}
