import { createHash } from 'node:crypto';

import { UserRole } from '../../common/enum';
import { ADMIN_EMAIL, ADMIN_PASSWORD } from '../../config';
import { AppDataSource } from '../../database/data-source';
import { User } from '../user.entity';

export const runSeed = async () => {
  const userRepository = AppDataSource.getRepository(User);

  const adminCount = await userRepository.count();

  if (adminCount === 0) {
    const newUser = await userRepository.create({
      username: 'superadmin',
      email: ADMIN_EMAIL,
      password: createHash('sha256').update(ADMIN_PASSWORD).digest('hex'),
      role: UserRole.Admin,
      createdBy: 1,
      updatedBy: 1
    });

    await userRepository.save(newUser);
  }
};
