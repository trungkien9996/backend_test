import {
  IsDate,
  IsEmail,
  IsInt,
  IsOptional,
  IsString,
  Length,
  validateOrReject
} from 'class-validator';
import { BeforeInsert, BeforeUpdate, Column, Entity, OneToMany } from 'typeorm';

import { BaseEntity } from '../common/entities';
import { EGender, UserRole } from '../common/enum';
import { setIsInvalidMessage } from '../common/helper/validation-message';
import { UserAttribute } from '../interfaces/user.interface';
import loggerFactory from '../lib/logger-factory';
import { Game } from './game.entity';

const logger = loggerFactory.getLogger(__filename);
@Entity()
export class User extends BaseEntity implements UserAttribute {
  @Column({ default: null })
  @IsOptional()
  fullname: string;

  @Column({ default: UserRole.Member, enum: UserRole, type: 'enum' })
  @IsInt()
  role: UserRole;

  @Column({ nullable: false, unique: true })
  @IsEmail(undefined, {
    message: setIsInvalidMessage('Email')
  })
  email: string;

  @Column({ nullable: false, unique: true })
  @Length(0, 20)
  username: string;

  @Column({ nullable: false })
  password: string;

  @Column({ default: null })
  @Length(9, 12)
  phone_number: string;

  @Column({ nullable: true, default: null })
  @IsOptional()
  @IsDate()
  last_access_at: Date;

  @Column({ default: EGender.Female, enum: EGender, type: 'enum' })
  @IsString()
  sex: EGender;

  @Column({ default: null })
  image: string;

  @OneToMany(() => Game, (game) => game.user)
  games: Game[];

  @BeforeInsert()
  @BeforeUpdate()
  async validate() {
    await validateOrReject(this, { skipUndefinedProperties: true }).catch((error) => {
      logger.error(error);
    });
  }
}
