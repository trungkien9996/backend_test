import { IsBoolean, IsInt, IsString, Length, validateOrReject } from 'class-validator';
import { BeforeInsert, BeforeUpdate, Column, Entity, ManyToOne } from 'typeorm';

import { BaseEntity } from '../common/entities';
import { GameAttribute } from '../interfaces/game.interface';
import loggerFactory from '../lib/logger-factory';
import { User } from './user.entity';

const logger = loggerFactory.getLogger(__filename);

@Entity()
export class Game extends BaseEntity implements GameAttribute {
  @ManyToOne(() => User, (user) => user.games)
  user: User;

  @Column({ nullable: false })
  @Length(5, 100)
  @IsString()
  name: string;

  @Column({ nullable: false })
  @IsInt()
  targetNumber: number;

  @Column({ default: 0 })
  @IsInt()
  attempts: number;

  @Column({ default: false })
  @IsBoolean()
  success: boolean;

  @BeforeInsert()
  @BeforeUpdate()
  async validate() {
    await validateOrReject(this, { skipUndefinedProperties: true }).catch((error) => {
      logger.log(error);
    });
  }
}
