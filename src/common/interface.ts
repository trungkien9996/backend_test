export interface BaseAttributes extends BaseTimeAttributes, BaseUserAttributes {
  id: number;
}

export interface BaseTimeAttributes {
  createdAt: Date;
  updatedAt: Date;
}

export interface BaseUserAttributes {
  createdBy?: number;
  updatedBy?: number;
}
