export const setIsInvalidMessage = (label: string): string => `${label} is invalid`;
