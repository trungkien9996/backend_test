export const Messages = {
  app: {},
  auth: {
    loginSuccess: 'đăng nhập thành công',
    loginRequire: 'yêu cầu đăng nhập',
    logoutSuccess: 'đăng xuất thành công'
  },
  cart: {
    name: 'giỏ hàng'
  },
  order: {
    name: 'đơn hàng',
    payment: 'thanh toán'
  },
  statusCode: {
    403: 'bạn không có quyền truy cập'
  },
  user: {}
};

export const isPublish = 'isPublish';

class MessagesService {
  removeAccent(text: string) {
    return text
      .normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '')
      .replace(/đ/g, 'd')
      .replace(/Đ/g, 'D');
  }

  setText(text: string) {
    return text.charAt(0).toUpperCase() + text.slice(1);
  }

  replaceText(rawString: string, replacements: Record<string, string>) {
    const keys = Object.keys(replacements);

    let replacedString = rawString;

    for (const key of keys) {
      replacedString = replacedString.replace(`{${key}}`, replacements[key]);
    }

    return replacedString;
  }

  setReplaceText(rawString: string, replacements: Record<string, string>) {
    return this.setText(this.replaceText(rawString, replacements));
  }

  setNotFound(document?: string) {
    return `Không tìm thấy ${document || 'tài liệu'}`;
  }

  setWrong(document?: string) {
    return `Sai ${document || 'tài liệu'}`;
  }

  setConflict(document?: string) {
    return `Trùng ${document || 'tài liệu'}`;
  }
}

export const messagesService = new MessagesService();
