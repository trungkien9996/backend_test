import { Type } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsNumberString, IsOptional } from 'class-validator';
import { JSONSchema } from 'class-validator-jsonschema';

export class FindAllDto {
  @IsOptional()
  @IsNumberString({}, { each: true })
  id?: string | string[];
}

export class UpdateDto {
  @IsNotEmpty()
  @IsNumber()
  readonly updatedBy: number;
}

export class CreateDto extends UpdateDto {
  @IsNotEmpty()
  @IsNumber()
  readonly createdBy: number;
}

export class Pagination {
  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  @JSONSchema({ type: 'number', example: 10 })
  limit = 10;

  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  @JSONSchema({ type: 'number', example: 1 })
  page = 1;
}
