export enum EGender {
  Male = 'male',
  Female = 'female',
  Unknown = 'unknown'
}

export enum UserRole {
  Admin,
  Member
}
