import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class BaseEntityWithoutUserData {
  @PrimaryGeneratedColumn({
    type: 'bigint'
  })
  id: number;

  @CreateDateColumn({ nullable: false })
  createdAt: Date;

  @CreateDateColumn({ nullable: false })
  updatedAt: Date;
}

@Entity()
export class BaseEntity extends BaseEntityWithoutUserData {
  @Column({ nullable: false, default: true })
  isActive: boolean;

  @Column({ nullable: false })
  createdBy?: number;

  @Column({ nullable: false })
  updatedBy?: number;
}
